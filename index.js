const express = require('express');
const cookieParser = require('cookie-parser');
const cors = require('cors');
// force https on online page
const app = express().use(function (req, res, next) {
	if (req.header('x-forwarded-proto') == 'http') {
		res.redirect(301, 'https://' + 'enotify.herokuapp.com' + req.url)
		return
	}
	next()
});
const server = require('https').createServer(app);
const porta = process.env.PORT || 3200;

// General Setup
app.use(express.json()); // to support JSON-encoded bodies
app.use(express.urlencoded({ // to support URL-encoded bodies
	extended: true
}));
app.use(cookieParser());
app.use(express.static(__dirname + '/public'));
//---

// Cors da aplicação
app.use(cors());

var originList = ['https://enotify.herokuapp.com']
var corsOptions = {
  origin: function (origin, callback) {
    if (originList.indexOf(origin) !== -1 || (!origin || origin.indexOf('//localhost:') > -1)) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS Policy'))
    }
  }
}
//---

//API Start and Sql Start
const sqlConnection = require('./routes/db');
const conSql = sqlConnection.sqlConfig();
const sendNotify = require('./routes/sendnotify');
const awsFiles = require('./routes/awsfiles.js');
//---

// HandleBars - Template Engine
const handleBars = require('express-handlebars');
const path = require('path');

app.set('views', path.join(__dirname, '/views'));
app.engine('handlebars', handleBars({
	defaultLayout: "index"
}));
app.set('view engine', 'handlebars');
//---

/** Pages Routes Website **/
app.get('/home', function (request, response) {
	response.render('home', {
		bodyId: "home",
		homepage: true,
		style: ['/css/style.css'],
		javascript: ['vendor/bootstrap-4.1/bootstrap.bundle.min.js', '/js/push/enotify.js', '/js/home.js']
	});
});

// Login Page
app.get('/login', function (request, response) {
	response.clearCookie('user');
	response.clearCookie('guid');
	response.render('login', {
		bodyId: "login",
		dashboard: false,
		style: ['/css/login.css'],
		javascript: []
	});
});

// Dashboard Home Page
app.get('/dashboard', function (request, response) {
	if (typeof request.cookies.user == 'undefined' && typeof request.cookies.guid == 'undefined')
		response.redirect('/login');
	else {
		response.render('dashboard', {
			bodyId: "dashboard",
			dashboard: true,
			style: ['/css/font-face.css', '/vendor/mdi-font/css/material-design-iconic-font.min.css', '/vendor/bootstrap-4.1/bootstrap.min.css', '/vendor/animsition/animsition.min.css',
			'/vendor/css-hamburgers/hamburgers.min.css','/css/dashboard-all.css','/css/theme.css'],
			javascript:	['/js/dashboard.js', '/vendor/bootstrap-4.1/popper.min.js', '/vendor/bootstrap-4.1/bootstrap.min.js', '/vendor/animsition/animsition.min.js'],
			userName: request.cookies.user,
			userGuid: request.cookies.guid,
			userEmail: request.cookies.email
		});
	}
});

// Product Page
app.get('/dashboard/product', function (request, response) {
	if (typeof request.cookies.user == 'undefined' && typeof request.cookies.guid == 'undefined')
		response.redirect('/login');
	else {
		response.render('product', {
			bodyId: "product",
			breadcrumb: "Registrar Produto",
			dashboard: true,
			style: ['/css/font-face.css', '/vendor/mdi-font/css/material-design-iconic-font.min.css', '/vendor/bootstrap-4.1/bootstrap.min.css', '/vendor/animsition/animsition.min.css',
			'/vendor/css-hamburgers/hamburgers.min.css', '/vendor/iconselect/iconselect.css', '/css/dashboard-all.css','/css/theme.css','/vendor/datetimepicker/bootstrap-datetimepicker.min.css'],
			javascript:	['/vendor/bootstrap-4.1/popper.min.js','/vendor/datetimepicker/moment.min.js','/vendor/datetimepicker/bootstrap-datetimepicker.min.js', '/vendor/bootstrap-4.1/bootstrap.min.js', '/vendor/animsition/animsition.min.js', 
			'/vendor/iconselect/iscroll.min.js','/vendor/iconselect/iconselect.js', '/js/push/enotify.js','/js/product.js'],
			idProd: request.query.idprod,
			userName: request.cookies.user,
			userGuid: request.cookies.guid,
			userEmail: request.cookies.email
		});
	}
});

// Documentation Page
app.get('/dashboard/services',function (request,response){
	if (typeof request.cookies.user == 'undefined' && typeof request.cookies.guid == 'undefined')
		response.redirect('/login');
	else{
		response.render('services', {
			bodyId: "services",
			breadcrumb: "Serviços",
			dashboard: true,
			style: ['/css/font-face.css', '/vendor/mdi-font/css/material-design-iconic-font.min.css', '/vendor/bootstrap-4.1/bootstrap.min.css', '/vendor/animsition/animsition.min.css',
			'/vendor/css-hamburgers/hamburgers.min.css','/css/dashboard-all.css','/css/theme.css'],
			javascript:	['/js/integration.js', '/vendor/bootstrap-4.1/popper.min.js', '/vendor/bootstrap-4.1/bootstrap.min.js', '/vendor/animsition/animsition.min.js'],
			userName: request.cookies.user,
			userEmail: request.cookies.email
		});
	}
});

// Integration Page
app.get('/dashboard/integration',function (request,response){
	if (typeof request.cookies.user == 'undefined' && typeof request.cookies.guid == 'undefined')
		response.redirect('/login');
	else{
		response.render('integration', {
			bodyId: "integration",
			breadcrumb: "Integração",
			dashboard: true,
			style: ['/css/font-face.css', '/vendor/mdi-font/css/material-design-iconic-font.min.css', '/vendor/bootstrap-4.1/bootstrap.min.css', '/vendor/animsition/animsition.min.css',
			'/vendor/css-hamburgers/hamburgers.min.css','/css/dashboard-all.css','/css/theme.css'],
			javascript:	['/js/integration.js', '/vendor/bootstrap-4.1/popper.min.js', '/vendor/bootstrap-4.1/bootstrap.min.js', '/vendor/animsition/animsition.min.js'],
			userName: request.cookies.user,
			userGuid: request.cookies.guid,
			userEmail: request.cookies.email
		});
	}
});

// app.get('/dashboard/config',function (request,response){
// 	if (typeof request.cookies.user == 'undefined' && typeof request.cookies.guid == 'undefined')
// 		response.redirect('/login');
// 	else{
// 		response.render('config', {
// 			bodyId: "config",
// 			dashboard: true,
// 			style: ['/css/font-face.css', '/vendor/mdi-font/css/material-design-iconic-font.min.css', '/vendor/bootstrap-4.1/bootstrap.min.css', '/vendor/animsition/animsition.min.css',
// 			'/vendor/css-hamburgers/hamburgers.min.css','/css/dashboard-all.css','/css/theme.css'],
// 			javascript:	['/js/integration.js', '/vendor/bootstrap-4.1/popper.min.js', '/vendor/bootstrap-4.1/bootstrap.min.js', '/vendor/animsition/animsition.min.js'],
// 			userName: request.cookies.user,
// 			userEmail: request.cookies.email
// 		});
// 	}
// });
/** END Routes **/

/** Login System Dashboard **/
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy;

app.use(passport.initialize());
app.use(passport.session());

passport.use(
	new LocalStrategy({
			usernameField: 'username',
			passwordField: 'password',
			guidField: 'guid',
			email: 'email',
			passReqToCallback: true
		},
		function (req, username, password, done) {
			var sql = "select * from users where client=?";

			conSql.query(sql, [username], function (err, result, fields) {
				if (err) {
					console.log('error sql')
					return done(err)
				} else if (result.length === 0) {
					return done(null, false)
				} else if (result[0].password != password) {
					return done(null, false)
				} else {
					return done(null, {
						username: username,
						guid: result[0].guid,
						email: result[0].email
					})
				}
			});
		}
	));

passport.serializeUser(function (user, done) {
	done(null, user);
});

passport.deserializeUser(function (user, done) {
	done(null, {
		username: username
	});
});

app.post('/login', passport.authenticate('local', {
	failureRedirect: '/login'
}), function (req, res) {
	if (req.isAuthenticated(req, res)) {
		res.cookie('user', req.user.username);
		res.cookie('guid', req.user.guid);
		res.cookie('email', req.user.email);
		res.redirect('/dashboard');
	}
});
/** END Login **/

/** Rota Consumo da API **/
app.get('/api/:userGUID', function (request, response) {
	sqlConnection.productApi(request, response);
})

app.get('/user/:userGUID', function (request, response) {
	sqlConnection.userConfig(request, response);
})

app.post('/sendnotify/:userGUID', function(request,response){
	sendNotify.cloudNotify(request, response);
})
/** END Consumo da API **/

/** Rota de Criação e Configurações de Usuário **/
app.get('/createstore/', function (request, response) {
	// /createstore?guid=DGUIDHERE&client=CLIENTNAMEHERE&password=PASSWORDHERE&host=HOSTHERE&email=EMAILHERE
	sqlConnection.createStore(request, response);
})

/** Rota de Criação/Alteração/Remoção dos Produtos de Cliente **/
app.post('/createproduct/', cors(corsOptions), function (request, response) {
	sqlConnection.createProduct(request, response)
})

app.post('/changeproduct/', cors(corsOptions), function (request, response) {
	// ?idprod=NUMBERPROD  //localhost:3200/dashboard/product  //localhost:3200/dashboard/product
	sqlConnection.changeProduct(request, response)
})

app.get('/deleteproduct/:idProd', cors(corsOptions), function (request, response) {
	sqlConnection.deleteProduct(request, response)
})

/** Integration FCM Page **/
app.get('/fcmcheck/:userGUID', cors(corsOptions), function (request, response) {
	sqlConnection.fcmcheck(request, response);
})

app.post('/fcmconfig', cors(corsOptions), function (request, response) {
	sqlConnection.fcmconfig(request, response);
})
/** END Configs **/

/** Upload Files **/
const mv = require('mv');
const formidable = require('formidable');

app.post('/uploadFile', cors(corsOptions), function (request, response) {
	var form = new formidable.IncomingForm();
	form.parse(request, function (err, fields, files) {
		if(files.img.type == 'image/jpeg') files.img.type = 'image/jpg';
		var fileName = request.headers.imgaction + '_' + request.headers.idphoto + '.' + files.img.type.replace('image/', '');
		var oldpath = files.img.path;
		var newpath = path.join(__dirname, '/public/upload/') + request.cookies.user + '/' + fileName;

		mv(oldpath, newpath, {mkdirp: true}, function (err) {
		 	if (err) {
		 		response.status(400).send({
		 			status: err
		 		});
		 	}
		 	response.status(200).send({
		 		status: 'Imagem salva com sucesso!',
		 		image: 'https://enotify.herokuapp.com/upload/' + request.cookies.user + '/' + fileName
			});
			awsFiles.awsFileUpload(newpath,request.cookies.user,fileName);
		});
	})
})

app.get('/updateapp/', function (request, response) {
	awsFiles.updateApp(request, response);
});
/** END Upload Files **/

//The 404 Route
app.get('*', function (request, response) {
	response.render('home', {
		bodyId: "home",
		homepage: true,
		style: ['/css/style.css'],
		javascript: ['vendor/bootstrap-4.1/bootstrap.bundle.min.js', '/js/push/enotify.js', '/js/home.js']
	});
});

app.listen(porta, function () {
	console.log("server on in: " + porta)
});