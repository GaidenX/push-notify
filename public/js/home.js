$(function() {
    "use strict";
    
    /*-----------------------------------
     * FIXED  MENU - HEADER
     *-----------------------------------*/
    function menuscroll() {
        var $navmenu = $('.nav-menu');
        if ($(window).scrollTop() > 50) {
            $navmenu.addClass('is-scrolling');
        } else {
            $navmenu.removeClass("is-scrolling");
        }
    }
    menuscroll();
    $(window).on('scroll', function() {
        menuscroll();
    });
    /*-----------------------------------
     * NAVBAR CLOSE ON CLICK
     *-----------------------------------*/

    $('.navbar-nav > li:not(.dropdown) > a').on('click', function() {
        $('.navbar-collapse').collapse('hide');
    });
    /* 
     * NAVBAR TOGGLE BG
     *-----------------*/
    var siteNav = $('#navbar');
    siteNav.on('show.bs.collapse', function(e) {
        $(this).parents('.nav-menu').addClass('menu-is-open');
    })
    siteNav.on('hide.bs.collapse', function(e) {
        $(this).parents('.nav-menu').removeClass('menu-is-open');
    })

    /*-----------------------------------
     * ONE PAGE SCROLLING
     *-----------------------------------*/
    // Select all links with hashes
    $('a[href*="#"]').not('[href="#"]').not('[href="#0"]').not('[data-toggle="tab"]').on('click', function(event) {
        // On-page links
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000, function() {
                    // Callback after animation
                    // Must change focus!
                    var $target = $(target);
                    $target.focus();
                    if ($target.is(":focus")) { // Checking if the target was focused
                        return false;
                    } else {
                        $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                        $target.focus(); // Set focus again
                    };
                });
            }
        }
    });
});

function demoPreview(demoType){
    'use strict';

    if(demoType=="simple")
    var message = {
        "product": "entrance",
        "tag": "simple-enotify",
        "link": "https://enotify.herokuapp.com",
        "icon": "/images/enotify-icon.png",
        "title" : "Demo Simples",
        "body": "Mensagem simples e objetiva pode ser uma ótima escolha de propaganda!",
        "timeout": "40000",
        "requireInteraction": false,
        "vibrate": [200, 200, 200, 200, 200]
    }
    else if(demoType=="complete")
    var message = {
        "product": "recomendation",
        "tag": "complete-enotify",
        "link": "https://enotify.herokuapp.com",
        "icon": "/images/enotify-icon.png",
        "image": "/images/site/connect.jpg", 
        "title" : "Demo Completa", 
        "body": "Notificação completa e chamativa para a sua propaganda, transforme sua mensagem em um poderoso marketing!",
        "requireInteraction": true,
        "vibrate": [200, 200, 200, 200, 200],
        "buttonview": "Veja"
    }

    Push.Permission.request();
    Push.Permission.has();
    Push.create(message.title, message);
}