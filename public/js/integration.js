function integrationPage(platform) {
    switch (platform) {
        case "blogger":
            $('#firebase-card, #firebase-info, #integration-card').hide();
            $('#blogger-card, #blogger-info').show();
            scrollIntegration();
            break;
        case "custom":
            $('#blogger-card, #blogger-info').hide();
            $('#firebase-card, #firebase-info, #integration-card').show();
            scrollIntegration();
            firebasePage();
            break;
    }
}

function scrollIntegration(){
    $('html,body').animate({
        scrollTop: $(".card").offset().top + $(".card").height() - 50
    }, 'slow');
}

var fcmAlreadyCall = false;

function firebasePage() {
    $('#fcmForm').on('submit', function (event) {
        event.preventDefault();
        let formData = $('#fcmForm').serializeArray();

        $.ajax({
            url: this.action,
            type: 'post',
            data: $(this).serialize(),
            success: function (data, textStatus) {
                    $('#modalButton').click();
                    $('#scriptBody').val(
                        'var config = {\n' +
                        '   apiKey: "' + formData[2].value + '",\n' +
                        '   authDomain: "' + formData[1].value + '.firebaseapp.com",\n' +
                        '   databaseURL: "https://' + formData[1].value + '.firebaseio.com",\n' +
                        '   projectId: "' + formData[1].value + '",\n' +
                        '   storageBucket: "' + formData[1].value + '.appspot.com",\n' +
                        '   messagingSenderId: "' + formData[1].value + '"\n' +
                        '};'
                    )
            },
            error: function (data) {
                alert(data.error);
            }
        })
    });

    // Verify if user already have a FCM and fill the fields
    if (!fcmAlreadyCall) {
        $.ajax({
            url: '/fcmcheck/' + readCookie("guid"),
            type: 'get',
            success: function (data, textStatus) {
                fcmAlreadyCall = true;
                $('#scriptBody').val(
                    'var config = {\n' +
                    '   apiKey: "' + data[0].fcmapi + '",\n' +
                    '   authDomain: "' + data[0].fcmproj + '.firebaseapp.com",\n' +
                    '   databaseURL: "https://' + data[0].fcmproj + '.firebaseio.com",\n' +
                    '   projectId: "' + data[0].fcmproj + '",\n' +
                    '   storageBucket: "' + data[0].fcmproj + '.appspot.com",\n' +
                    '   messagingSenderId: "' + data[0].fcmsender + '"\n' +
                    '};'
                )
            },
            error: function (data) {
                fcmAlreadyCall = true;
            }
        });
    }
}