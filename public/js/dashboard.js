(function () {
  let sessionGuid = readCookie("guid");

  $.ajax({
    url: '/api/' + sessionGuid + '',
    type: 'get',
    success: function (data, textStatus) {
      if (textStatus === 'success') {
        let tableContent = function () {
          var htmlTable = '';
          for (var key in data) {
            //Translate for html constructor
            switch(data[key].device){
              case "both": data[key].device = "ambos";break;
              case "mobile": data[key].device = "celulares";break;
              case "desktop": data[key].device = "computadores";break;
            }
            
            htmlTable += '<tr class="tr-shadow"><td>' + data[key].name + '</td>' +
              '<td><span class="block-product">' + data[key].product + '</span></td>' +
              '<td class="link">' + data[key].host + '</td>' +
              '<td>' + data[key].title + '</td>' +
              '<td>' + data[key].device + '</td>' +
              '<td><span class="status-process" data-status="'+data[key].status+'">' + data[key].status + '</span></td>' +
              '<td><div class="table-data-feature">' +
              '<a class="item" href="' + window.location.href + '/product?idprod=' + data[key].id + '" data-toggle="tooltip" data-placement="top" title="Editar"><i class="zmdi zmdi-edit"></i></a>' +
              '<button class="item" data-toggle="tooltip" data-placement="top" title="Deletar" onclick="confirmModal(' + data[key].id + ')"><i class="zmdi zmdi-delete"></i></button>' +
              '</td></tr><tr class="spacer"></tr>';
          }
          return htmlTable;
        }
  
        // document.querySelectorAll('.statistic__item h2')[0].innerHTML = data.length;
        document.getElementsByClassName('productTable')[0].innerHTML = tableContent();
      }
    },
    error: function (data) {
      console.warn("Request eNotify Service", data);
      document.getElementsByClassName('productTable')[0].innerHTML = '<tr class="tr-shadow"><td align="center" colspan="7">Nenhum produto cadastrado ou encontrado.</td></tr>';
    }
  });
})();

/*** Product Table Actions Buttons ***/
function confirmModal(idProd){
    $('#modalButton').click();
    $('#confirm-modal').attr('data-prod',idProd)
}

function removeProd(element) {
  $.ajax({
    url: '/deleteproduct/'+$(element).attr('data-prod'),
    type: 'get',
    success: function (data, textStatus) {
      if (textStatus === 'success') {
        location.reload()
      }
    },
    error: function (data) {
      alert(data.error);
    }
  });
}