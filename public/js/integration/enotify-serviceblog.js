'use strict';
(function(global,factory){typeof exports==='object'&&typeof module!=='undefined'?module.exports=factory():typeof define==='function'&&define.amd?define(factory):(global.Push=factory())}(this,(function(){'use strict';function _typeof(obj){if(typeof Symbol==="function"&&typeof Symbol.iterator==="symbol"){_typeof=function(obj){return typeof obj}}else{_typeof=function(obj){return obj&&typeof Symbol==="function"&&obj.constructor===Symbol&&obj!==Symbol.prototype?"symbol":typeof obj}}
return _typeof(obj)}
function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function")}}
function _defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||!1;descriptor.configurable=!0;if("value" in descriptor)descriptor.writable=!0;Object.defineProperty(target,descriptor.key,descriptor)}}
function _createClass(Constructor,protoProps,staticProps){if(protoProps)_defineProperties(Constructor.prototype,protoProps);if(staticProps)_defineProperties(Constructor,staticProps);return Constructor}
function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function")}
subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:!1,writable:!0,configurable:!0}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass}
function _possibleConstructorReturn(self,call){if(call&&(typeof call==="object"||typeof call==="function")){return call}
if(self===void 0){throw new ReferenceError("this hasn't been initialised - super() hasn't been called")}
return self}
var errorPrefix='PushError:';var Messages={errors:{incompatible:"".concat(errorPrefix," Push.js is incompatible with browser."),invalid_plugin:"".concat(errorPrefix," plugin class missing from plugin manifest (invalid plugin). Please check the documentation."),invalid_title:"".concat(errorPrefix," title of notification must be a string"),permission_denied:"".concat(errorPrefix," permission request declined"),sw_notification_error:"".concat(errorPrefix," could not show a ServiceWorker notification due to the following reason: "),sw_registration_error:"".concat(errorPrefix," could not register the ServiceWorker due to the following reason: "),unknown_interface:"".concat(errorPrefix," unable to create notification: unknown interface")}};var Permission=function(){function Permission(win){_classCallCheck(this,Permission);this._win=win;this.GRANTED='granted';this.DEFAULT='default';this.DENIED='denied';this._permissions=[this.GRANTED,this.DEFAULT,this.DENIED]}
_createClass(Permission,[{key:"request",value:function request(onGranted,onDenied){return arguments.length>0?this._requestWithCallback.apply(this,arguments):this._requestAsPromise()}},{key:"_requestWithCallback",value:function _requestWithCallback(onGranted,onDenied){var _this=this;var existing=this.get();var resolve=function resolve(){var result=arguments.length>0&&arguments[0]!==undefined?arguments[0]:_this._win.Notification.permission;if(typeof result==='undefined'&&_this._win.webkitNotifications)result=_this._win.webkitNotifications.checkPermission();if(result===_this.GRANTED||result===0){if(onGranted)onGranted()}else if(onDenied)onDenied()};if(existing!==this.DEFAULT){resolve(existing)}else if(this._win.webkitNotifications&&this._win.webkitNotifications.checkPermission){this._win.webkitNotifications.requestPermission(resolve)}else if(this._win.Notification&&this._win.Notification.requestPermission){this._win.Notification.requestPermission().then(resolve).catch(function(){if(onDenied)onDenied()})}else if(onGranted){onGranted()}}},{key:"_requestAsPromise",value:function _requestAsPromise(){var _this2=this;var existing=this.get();var isGranted=function isGranted(result){return result===_this2.GRANTED||result===0};var hasPermissions=existing!==this.DEFAULT;var isModernAPI=this._win.Notification&&this._win.Notification.requestPermission;var isWebkitAPI=this._win.webkitNotifications&&this._win.webkitNotifications.checkPermission;return new Promise(function(resolvePromise,rejectPromise){var resolver=function resolver(result){return isGranted(result)?resolvePromise():rejectPromise()};if(hasPermissions){resolver(existing)}else if(isWebkitAPI){_this2._win.webkitNotifications.requestPermission(function(result){resolver(result)})}else if(isModernAPI){_this2._win.Notification.requestPermission().then(function(result){resolver(result)}).catch(rejectPromise)}else resolvePromise()})}},{key:"has",value:function has(){return this.get()===this.GRANTED}},{key:"get",value:function get(){var permission;if(this._win.Notification&&this._win.Notification.permission)permission=this._win.Notification.permission;else if(this._win.webkitNotifications&&this._win.webkitNotifications.checkPermission)
permission=this._permissions[this._win.webkitNotifications.checkPermission()];else if(navigator.mozNotification)
permission=this.GRANTED;else if(this._win.external&&this._win.external.msIsSiteMode)
permission=this._win.external.msIsSiteMode()?this.GRANTED:this.DEFAULT;else permission=this.GRANTED;return permission}}]);return Permission}();var Util=function(){function Util(){_classCallCheck(this,Util)}
_createClass(Util,null,[{key:"isUndefined",value:function isUndefined(obj){return obj===undefined}},{key:"isNull",value:function isNull(obs){return obj===null}},{key:"isString",value:function isString(obj){return typeof obj==='string'}},{key:"isFunction",value:function isFunction(obj){return obj&&{}.toString.call(obj)==='[object Function]'}},{key:"isObject",value:function isObject(obj){return _typeof(obj)==='object'}},{key:"objectMerge",value:function objectMerge(target,source){for(var key in source){if(target.hasOwnProperty(key)&&this.isObject(target[key])&&this.isObject(source[key])){this.objectMerge(target[key],source[key])}else{target[key]=source[key]}}}}]);return Util}();var AbstractAgent=function AbstractAgent(win){_classCallCheck(this,AbstractAgent);this._win=win};var DesktopAgent$$1=function(_AbstractAgent){_inherits(DesktopAgent$$1,_AbstractAgent);function DesktopAgent$$1(){_classCallCheck(this,DesktopAgent$$1);return _possibleConstructorReturn(this,(DesktopAgent$$1.__proto__||Object.getPrototypeOf(DesktopAgent$$1)).apply(this,arguments))}
_createClass(DesktopAgent$$1,[{key:"isSupported",value:function isSupported(){return this._win.Notification!==undefined}},{key:"create",value:function create(title,options){var buttonActions=[];if(options.product=='recomendation'||options.product=='wayout'){(options.buttonview!=''&&options.buttonview!=null)?buttonActions.push({action:'view','icon':options.iconview,'title':options.buttonview}):''}
return new this._win.Notification(title,{icon:Util.isString(options.icon)||Util.isUndefined(options.icon)||Util.isNull(options.icon)?options.icon:options.icon.x32,body:options.body,actions:buttonActions,image:options.image,tag:options.tag,requireInteraction:options.requireInteraction})}},{key:"close",value:function close(notification){notification.close()}}]);return DesktopAgent$$1}(AbstractAgent);var MobileChromeAgent$$1=function(_AbstractAgent){_inherits(MobileChromeAgent$$1,_AbstractAgent);function MobileChromeAgent$$1(){_classCallCheck(this,MobileChromeAgent$$1);return _possibleConstructorReturn(this,(MobileChromeAgent$$1.__proto__||Object.getPrototypeOf(MobileChromeAgent$$1)).apply(this,arguments))}
_createClass(MobileChromeAgent$$1,[{key:"isSupported",value:function isSupported(){return this._win.navigator!==undefined&&this._win.navigator.serviceWorker!==undefined}},{key:"getFunctionBody",value:function getFunctionBody(func){var str=func.toString().match(/function[^{]+{([\s\S]*)}$/);return typeof str!=='undefined'&&str!==null&&str.length>1?str[1]:null}},{key:"create",value:function create(id,title,options,serviceWorker,callback){var _this=this;this._win.navigator.serviceWorker.register(serviceWorker);this._win.navigator.serviceWorker.ready.then(function(registration){var buttonActions=[];if(options.product=='recomendation'||options.product=='wayout'){(options.buttonview!=''&&options.buttonview!=null)?buttonActions.push({action:'view','icon':options.iconview,'title':options.buttonview}):''}
var localData={id:id,link:options.link,purchase:(options.purchaseurl!='undefined'||options.purchaseurl!=''||options.purchaseurl!=null)?options.purchaseurl:'',origin:document.location.href,onClick:Util.isFunction(options.onClick)?_this.getFunctionBody(options.onClick):'',onClose:Util.isFunction(options.onClose)?_this.getFunctionBody(options.onClose):''};if(options.data!==undefined&&options.data!==null)localData=Object.assign(localData,options.data);registration.showNotification(title,{icon:options.icon,body:options.body,actions:buttonActions,image:(options.product=='recomendation'||options.product=='wayout')?options.image:'',vibrate:options.vibrate,tag:"enotify-preview",data:localData,requireInteraction:options.requireInteraction,silent:options.silent}).then(function(){registration.getNotifications().then(function(notifications){registration.active.postMessage('');callback(notifications)})}).catch(function(error){throw new Error(Messages.errors.sw_notification_error+error.message)})}).catch(function(error){throw new Error(Messages.errors.sw_registration_error+error.message)})}},{key:"close",value:function close(){}}]);return MobileChromeAgent$$1}(AbstractAgent);var MobileFirefoxAgent$$1=function(_AbstractAgent){_inherits(MobileFirefoxAgent$$1,_AbstractAgent);function MobileFirefoxAgent$$1(){_classCallCheck(this,MobileFirefoxAgent$$1);return _possibleConstructorReturn(this,(MobileFirefoxAgent$$1.__proto__||Object.getPrototypeOf(MobileFirefoxAgent$$1)).apply(this,arguments))}
_createClass(MobileFirefoxAgent$$1,[{key:"isSupported",value:function isSupported(){return this._win.navigator.mozNotification!==undefined}},{key:"create",value:function create(title,options){var notification=this._win.navigator.mozNotification.createNotification(title,options.body,options.icon);notification.show();return notification}}]);return MobileFirefoxAgent$$1}(AbstractAgent);var MSAgent$$1=function(_AbstractAgent){_inherits(MSAgent$$1,_AbstractAgent);function MSAgent$$1(){_classCallCheck(this,MSAgent$$1);return _possibleConstructorReturn(this,(MSAgent$$1.__proto__||Object.getPrototypeOf(MSAgent$$1)).apply(this,arguments))}
_createClass(MSAgent$$1,[{key:"isSupported",value:function isSupported(){return this._win.external!==undefined&&this._win.external.msIsSiteMode!==undefined}},{key:"create",value:function create(title,options){this._win.external.msSiteModeClearIconOverlay();this._win.external.msSiteModeSetIconOverlay(Util.isString(options.icon)||Util.isUndefined(options.icon)?options.icon:options.icon.x16,title);this._win.external.msSiteModeActivate();return null}},{key:"close",value:function close(){this._win.external.msSiteModeClearIconOverlay()}}]);return MSAgent$$1}(AbstractAgent);var WebKitAgent$$1=function(_AbstractAgent){_inherits(WebKitAgent$$1,_AbstractAgent);function WebKitAgent$$1(){_classCallCheck(this,WebKitAgent$$1);return _possibleConstructorReturn(this,(WebKitAgent$$1.__proto__||Object.getPrototypeOf(WebKitAgent$$1)).apply(this,arguments))}
_createClass(WebKitAgent$$1,[{key:"isSupported",value:function isSupported(){return this._win.webkitNotifications!==undefined}},{key:"create",value:function create(title,options){var notification=this._win.webkitNotifications.createNotification(options.icon,title,options.body);notification.show();return notification}},{key:"close",value:function close(notification){notification.cancel()}}]);return WebKitAgent$$1}(AbstractAgent);var Push$$1=function(){function Push$$1(win){_classCallCheck(this,Push$$1);this._currentId=0;this._notifications={};this._win=win;this.Permission=new Permission(win);this._agents={desktop:new DesktopAgent$$1(win),chrome:new MobileChromeAgent$$1(win),firefox:new MobileFirefoxAgent$$1(win),ms:new MSAgent$$1(win),webkit:new WebKitAgent$$1(win)};this._configuration={serviceWorker:'/service-worker.js',fallback:function fallback(payload){}}}
_createClass(Push$$1,[{key:"_closeNotification",value:function _closeNotification(id){var success=!0;var notification=this._notifications[id];if(notification!==undefined){success=this._removeNotification(id);if(this._agents.desktop.isSupported())this._agents.desktop.close(notification);else if(this._agents.webkit.isSupported())
this._agents.webkit.close(notification);else if(this._agents.ms.isSupported())
this._agents.ms.close();else{success=!1;throw new Error(Messages.errors.unknown_interface)}
return success}
return!1}},{key:"_addNotification",value:function _addNotification(notification){var id=this._currentId;this._notifications[id]=notification;this._currentId++;return id}},{key:"_removeNotification",value:function _removeNotification(id){var success=!1;if(this._notifications.hasOwnProperty(id)){delete this._notifications[id];success=!0}
return success}},{key:"_prepareNotification",value:function _prepareNotification(id,options){var _this=this;var wrapper;wrapper={get:function get(){return _this._notifications[id]},close:function close(){_this._closeNotification(id)}};if(options.timeout){setTimeout(function(){wrapper.close()},options.timeout)}
return wrapper}},{key:"_serviceWorkerCallback",value:function _serviceWorkerCallback(notifications,options,resolve){var _this2=this;var id=this._addNotification(notifications[notifications.length-1]);if(navigator&&navigator.serviceWorker){navigator.serviceWorker.addEventListener('message',function(event){var data=JSON.parse(event.data);if(data.action==='close'&&Number.isInteger(data.id))_this2._removeNotification(data.id)});resolve(this._prepareNotification(id,options))}
resolve(null)}},{key:"_createCallback",value:function _createCallback(title,options,resolve){var _this3=this;var notification=null;var onClose;options=options||{};onClose=function onClose(id){_this3._removeNotification(id);if(Util.isFunction(options.onClose)){options.onClose.call(_this3,notification)}};if(this._agents.desktop.isSupported()){try{notification=this._agents.desktop.create(title,options)}catch(e){var id=this._currentId;var sw=this.config().serviceWorker;var cb=function cb(notifications){return _this3._serviceWorkerCallback(notifications,options,resolve)};if(this._agents.chrome.isSupported()){this._agents.chrome.create(id,title,options,sw,cb)}}}else if(this._agents.webkit.isSupported())notification=this._agents.webkit.create(title,options);else if(this._agents.firefox.isSupported())
this._agents.firefox.create(title,options);else if(this._agents.ms.isSupported())
notification=this._agents.ms.create(title,options);else{options.title=title;this.config().fallback(options)}
if(notification!==null){var _id=this._addNotification(notification);var wrapper=this._prepareNotification(_id,options);if(Util.isFunction(options.onShow))notification.addEventListener('show',options.onShow);if(Util.isFunction(options.onError))notification.addEventListener('error',options.onError);if(Util.isFunction(options.onClick))notification.addEventListener('click',options.onClick);notification.addEventListener('close',function(){onClose(_id)});notification.addEventListener('cancel',function(){onClose(_id)});resolve(wrapper)}
resolve(null)}},{key:"create",value:function create(title,options){var _this4=this;var promiseCallback;if(!Util.isString(title)){throw new Error(Messages.errors.invalid_title)}
if(!this.Permission.has()){promiseCallback=function promiseCallback(resolve,reject){_this4.Permission.request().then(function(){_this4._createCallback(title,options,resolve)}).catch(function(){reject(Messages.errors.permission_denied)})}}else{promiseCallback=function promiseCallback(resolve,reject){try{_this4._createCallback(title,options,resolve)}catch(e){reject(e)}}}
return new Promise(promiseCallback)}},{key:"count",value:function count(){var count=0;var key;for(key in this._notifications){if(this._notifications.hasOwnProperty(key))count++}
return count}},{key:"close",value:function close(tag){var key,notification;for(key in this._notifications){if(this._notifications.hasOwnProperty(key)){notification=this._notifications[key];if(notification.tag===tag){return this._closeNotification(key)}}}}},{key:"clear",value:function clear(){var key,success=!0;for(key in this._notifications){if(this._notifications.hasOwnProperty(key))success=success&&this._closeNotification(key)}
return success}},{key:"supported",value:function supported(){var supported=!1;for(var agent in this._agents){if(this._agents.hasOwnProperty(agent))supported=supported||this._agents[agent].isSupported()}
return supported}},{key:"config",value:function config(settings){if(typeof settings!=='undefined'||settings!==null&&Util.isObject(settings))Util.objectMerge(this._configuration,settings);return this._configuration}},{key:"extend",value:function extend(manifest){var plugin,Plugin,hasProp={}.hasOwnProperty;if(!hasProp.call(manifest,'plugin')){throw new Error(Messages.errors.invalid_plugin)}else{if(hasProp.call(manifest,'config')&&Util.isObject(manifest.config)&&manifest.config!==null){this.config(manifest.config)}
Plugin=manifest.plugin;plugin=new Plugin(this.config());for(var member in plugin){if(hasProp.call(plugin,member)&&Util.isFunction(plugin[member]))
this[member]=plugin[member]}}}}]);return Push$$1}();var index=new Push$$1(typeof window!=='undefined'?window:global);return index})))

/*************************
 *****  Check Mobile  *****
 *************************/
function innix_checkMobile() {
    var check = false;
    (function (a) {
        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4)))
            check = true;
    })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
}

function innixCreateCookie(idProd) {
    'use strict';
    let byPassCookie = true;
    if (innixReadCookie('innixProdOpen') != null) {
        let cokkieArray = innixReadCookie('innixProdOpen').split(',');
        for (var key in cokkieArray) {
            if (idProd == cokkieArray[key]) {
                byPassCookie = false;
                return false;
            }
        }
    }
    if (byPassCookie) {
        var now = new Date();
        var expireTime = now.getTime() + 1 * 3600 * 1000;
        now.setTime(expireTime);
        var cookieContent = (innixReadCookie('innixProdOpen') != null) ? innixReadCookie('innixProdOpen') + ',' + idProd : idProd;
        document.cookie = 'innixProdOpen=' + cookieContent + ';expires=' + now.toGMTString() + ';path=/';
        return true;
    }
}

function innixReadCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1, c.length);
        }
        if (c.indexOf(nameEQ) === 0) {
            return c.substring(nameEQ.length, c.length);
        }
    }
    return null;
}

/*************************
 ******  Aplication  ******
 *************************/
(function () {
    let scripts = document.getElementsByTagName('script');
    let innixId, fcmProj, fcmApi, fcmToken, fcmSender;

    // Utm Constructor
    var utmLogic = function(link,utm){
        let newUrl;
        switch(true){
            case(link.slice(-1) == '?'):
                newUrl = link + utm;
            break;
            case(link.indexOf('?') > -1):
                newUrl = link + '&' + utm;
            break;
            case(link.indexOf('?') == -1):
                newUrl = (link.slice(-1) == '/') ? link + '?' + utm : link + '/?' + utm;
            break;
        }
        return newUrl;
    }

    for (var key in scripts) {
        if (typeof scripts[key].src != "undefined")
            if ((scripts[key].src).indexOf('enotify-serviceblog.js') > -1){
                innixId = scripts[key].id;
            }
    }

    if (innixId != null) {
        'use strict'
        // Get User Products Api
        const Http = new XMLHttpRequest();
        const url = 'https://enotify.herokuapp.com/api/' + innixId + '';
        Http.open("GET", url, true);
        Http.send();
        Http.onreadystatechange = function () {
            console.log('Requisição feita');
            // if request true - user exist
            if (this.readyState == 4 && this.status == 200) {
                let Notifier = JSON.parse(Http.response);
                var notifierConfig = [];

                for (var key in Notifier) {
                    notifierConfig.push(Notifier[key]);
                    let byPass;

                    // notifierConfig[key].onClick = function(){window.focus();this.close();}
                    notifierConfig[key].requireInteraction = (notifierConfig[key].timeout == null || notifierConfig[key].timeout == "" || notifierConfig[key].timeout == 0) ? true : false;
                    notifierConfig[key].vibrate = [200, 200, 200, 200, 200];
                    notifierConfig[key].image = notifierConfig[key].image;
                    switch (notifierConfig[key].device) {
                        case "desktop":
                            byPass = (!innix_checkMobile()) ? true : false;
                            break;
                        case "mobile":
                            byPass = (innix_checkMobile()) ? true : false;
                            break;
                        default:
                            byPass = true;
                    }
                    console.log(notifierConfig[key]);
                    // UTM
                    if(notifierConfig[key].utmstate == 'true') notifierConfig[key].link = utmLogic(notifierConfig[key].link,notifierConfig[key].utm);

                    // Product Entrance
                    if (notifierConfig[key].status == 'on' && byPass && window.location.href.indexOf(notifierConfig[key].host) > -1) {
                        if (innixCreateCookie(notifierConfig[key].id)) {
                                Push.Permission.request();
                                Push.Permission.has();
                                Push.create(notifierConfig[key].title, notifierConfig[key]);
                        }
                    }
                    }
            } else if (this.readyState == 4 && this.status == 404) {
                console.warn("Request InnixService", JSON.parse(Http.response));
            }
        }
    }
})();