var iconSelect, selectedText;

(function () {
    selectedText = document.getElementById('iconview');
            
    document.getElementById('my-icon-select').addEventListener('changed', function(e){
        selectedText.value = iconSelect.getSelectedValue();
    });
            
    iconSelect = new IconSelect("my-icon-select");

    var icons = [];
    icons.push({'iconFilePath':'/images/client-icon/none-icon.png', 'iconValue':''});
    icons.push({'iconFilePath':'/images/client-icon/eye-icon.png', 'iconValue':'https://enotify.herokuapp.com/images/client-icon/eye-icon.png'});
    icons.push({'iconFilePath':'/images/client-icon/chat-icon.png', 'iconValue':'https://enotify.herokuapp.com/images/client-icon/chat-icon.png'});
    icons.push({'iconFilePath':'/images/client-icon/heart-icon.png', 'iconValue':'https://enotify.herokuapp.com/images/client-icon/heart-icon.png'});
            
    iconSelect.refresh(icons);


    $("#status").on('change',function(){
        $(this).prop('checked') ? $('.custom-control-label').addClass('active') : $('.custom-control-label').removeClass('active');
    })
    $('#datetimepicker').datetimepicker({format: 'HH:mm'});
    $('#datetimepicker').data("DateTimePicker").date(moment(new Date).format('HH:mm'));

    $(document).on('focus', '#body', function(e){UpdateCount($(this))});
    $(document).on('keyup', '#body', function(e){UpdateCount($(this))});

    //Files Img inputs
    $(".image-file").change(function(){
        sendImage($(this).attr('id'),$(this).attr('data-prod'),$(this).attr('name'));
    });

    //UTM inputs
    $('input[type=radio][name=utmStatus]').change(function () {
        if (this.value == 'true')
            $('#utmForm').show(500);
        else
            $('#utmForm').hide(500);
    });

    // Change Product Area
    var urlParam = new URL(window.location.href).searchParams.get("idprod");
    if (urlParam != null && urlParam != "undefined") {
        let sessionGuid = readCookie("guid");
        $.ajax({
            url: '/api/' + sessionGuid + '',
            type: 'get',
            cache: false,
            success: function (data, err) {
                var productInfo = data.find(item => item.id === parseInt(urlParam));

                //Filling form informations
                $("#name").val(productInfo.name);
                $("#type").val(1).change();
                $("#type").val(productInfo.product).change();
                let status = (productInfo.status === "on") ? true : false;
                $("#status").prop('checked', status);
                if(productInfo.status === "on") $('.custom-control-label').addClass('active')
                $("#host").val(productInfo.host);
                $("#device").val(productInfo.device).change();
                $("#icon").val(productInfo.icon);
                $("#image").val(productInfo.image);
                $("#link").val(productInfo.link);
                $("#timeout").val(productInfo.timeout);
                $("#title").val(productInfo.title);
                $("#body").val(productInfo.body);
                if(productInfo.utmstate == true || productInfo.utmstate == 'true'){$('#utmConfirm').prop('checked', true); $('#utmUnconfirm').prop('checked', false)}
                $("#view").val(productInfo.buttonview);
                $('.icon img').each(function() {
                    if($(this).attr('icon-value') == productInfo.iconview){
                        $(this).click();
                    }
                });
                
                // $("#purchase").val(productInfo.buttonpurchase);
                // $("#purchaseUrl").val(productInfo.purchaseurl);

                UpdateCount($('#body'));
            }
        })
    }

    // Submit form and clear if pass 200 response
    $('#productForm').on('submit', function (event) {
        event.preventDefault();

        // Determine the post Url
        if (urlParam != null && urlParam != "undefined") var urlPost = '/changeproduct/';
        else var urlPost = '/createproduct/';

        $.ajax({
            url: urlPost,
            type: 'post',
            data: $(this).serialize(),
            success: function (data, textStatus) {
                if (urlParam == null && urlParam == "undefined") $('#productForm').trigger("reset");
                $('#smallmodal .modal-body').html('<p>Produto cadastrado com sucesso!</p>');
                $('#modalButton').click();
            },
            error: function(data) {
                $('#smallmodal .modal-body').html('<p>Não foi possível registrar o produto. Verifique e tente novamente.</p>');
                $('#modalButton').click();
            }
        })
    });
})();

function UpdateCount(textArea) {
    var msgSpan = $('#countMsg');
    var ml     = parseInt(textArea.attr('maxlength'));
    var length = textArea.val().length;
    var msg = ml - length + ' caracteres restantes.';
    msgSpan.html(msg);
  }

/*************************
 ******  Aplication  ******
 *************************/
function pushPreview(){
    'use strict';
    var unindexed_array = $('#productForm').serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

    console.log(indexed_array);

    var notifierConfig = {
        "name": indexed_array.name,
        "product": indexed_array.type,
        "link": indexed_array.link,
        "icon": indexed_array.icon,
        "image": indexed_array.image,  
        "body": indexed_array.body,
        "title" : indexed_array.title,
        "timeout": parseInt(indexed_array.timeout),
        "requireInteraction": (indexed_array.timeout == "0") ? true : false,
        "vibrate": [200, 200, 200, 200, 200],
        "iconview": indexed_array.iconview,
        "buttonview": indexed_array.view
        // "buttonpurchase": indexed_array.purchase,
        // "purchaseurl": indexed_array.purchaseUrl
    }

    Push.Permission.request();
    Push.Permission.has();
    Push.create(notifierConfig.title, notifierConfig);
}

function sendImage(idInput,idImg,actionImg) {
    var photo = document.getElementById(idInput).files[0];

    if (typeof photo != "undefined" || photo != null) {
        var formData = new FormData();
        formData.append("img", photo);
        $.ajax({
            url: '/uploadFile',
            type: 'post',
            contentType: false,
            processData: false,
            headers: {
                "idphoto": idImg,
                "imgaction": actionImg
            },
            data: formData,
            success: function (data) {
                if (idInput == "headerUpload")
                    $('#image').val(data.image);
                else if (idInput == "iconUpload")
                    $('#icon').val(data.image);
            },
            error: function (err) {
                console.log(err);
            }
        })
    }
}