# Enotify

**Projeto do Innix Enotify** ( Site / Dashboard / Backend API ).

Enotify é um serviço **PWA de Notificações Push** para ecommerce e blogs, utilizando também as tecnologias do **Firebase**. No dashboard o usuário logado pode criar/editar/gerenciar suas campanhas de notificações que apareceram em seu site. Os arquivos de integração são responsável por deixar a aplicação de forma automática e simples para o usuário.

O banco de dados é em **MySql** e um **bucket AWS S3** somente para arquivos upload dos clientes.

### Especificações

* **Server-side >** NodeJs, HandleBars Engine Template, Firebase.
* **Client-side >** HandleBars, HTML, CSS, Bootstrap 4, JavaScript/jQuery.
* **Arquivos de Integração >** Javascript e Firebase.
* **Banco de dados >** MySql e AWS bucket S3

## Acesso

**Caso necessário pedir autorização para acessar os servidores ou uma conta teste para acesso ao dashboard no ar:**

[Acesse o site da Aplicação](https://enotify.herokuapp.com/)

[Acesse o dashboard](https://enotify.herokuapp.com/login)

# Url's criadas até o momento e suas funções

    /api/:userGUID => Url de consumo do serviço API para a integração do Notify

    /login => area de acesso do usuário ao dashboard

    /dashboard => area do dashboard, se não tiver logado será redirecionado a página de Login

    /createstore/?params => Url para criação do cliente, necessário passar parametros a mão para o cadastro * ?guid=DGUIDHERE&client=CLIENTNAMEHERE&password=PASSWORDHERE&host=HOSTHERE&email=EMAILHERE
    
    /dashboard/product => Url de dashboard para a criação e edição de um produto. para edição é usado o parametro * ?idprod=IDPROD
    
    /dashboard/services => Página Dashboard com as descrições dos produtos e dicas para uso
    
    /dashboard/integration => Página Dashboard para cadastro do FCM para o Firebase, download dos arquivos e guia para a integração
    
## Update App

https://enotify.herokuapp.com/updateapp/ => Esta URL é a responsável pela atualização geral de arquivos dentro do dashboard. Em caso de algum erro ou falta de um arquivo por exemplo, executar esta url para fazer um "restore" da aplicação com todos os dados e arquivos salvos.

## Deploy

Deploy em servidores da Heroku App e MySql hospedado no servidor próprio da Heroku (db add-on).

[Versão em Live](https://enotify.herokuapp.com/)

## Build

* [Heroku App CLI NodeJs](https://devcenter.heroku.com/articles/heroku-cli)