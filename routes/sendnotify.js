const express = require('express');
const app = express();
const cUrl = require('request');
const schedule = require('node-schedule');

exports.cloudNotify = function(request, response){
    let str = request.body.notification.schedulehour;
	let schedulehour = str.split(":").map(val => Number(val));

	let message = {
		"to": ""+request.body.to,
		"priority": "high",
		"data": {
			"id": "uniqueid",
			"icon": request.body.notification.icon,
			"title": request.body.notification.title,
			"body": request.body.notification.body,
			"image": request.body.notification.image,
			"requireInteraction": request.body.notification.requireInteraction,
			"link": request.body.notification.link
		}
	};

	let clientServerOptions = {
		uri: 'https://fcm.googleapis.com/fcm/send',
		body: JSON.stringify(message),
		method: 'POST',
		dataType: 'json',
		headers: {
			'Authorization': request.body.authorization,
			'Content-Type': 'application/json'
		}
	}

	console.log(schedulehour);

	var rule = new schedule.RecurrenceRule();
	rule.dayOfWeek = [0, new schedule.Range(1, 5)];
	rule.hour = schedulehour[0];
	rule.minute = schedulehour[1];

	var j = schedule.scheduleJob(rule, function () {
	 	cUrl.post(clientServerOptions, (error, res, body) => {
	 		if (error) {
	 			console.error(error)
	 			return
	 		}
	 		console.log(`statusCode: ${res.statusCode}`)
	 		console.log(body)
	 	})
	});

	response.status(200).send({
		status: 'Produto agendado com sucesso'
	});
}