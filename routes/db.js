const express = require('express');
const app = express();
const mysql = require('mysql');
const path = require('path');
const config = require('../config/config.json');

app.use(express.json()); // to support JSON-encoded bodies
app.use(express.urlencoded({ // to support URL-encoded bodies
	extended: true
}));

//Config SQL
var conSql = mysql.createConnection({
	host: config.Mysql.host,
	port: config.Mysql.port,
	user: config.Mysql.user,
	password: config.Mysql.password,
	database: config.Mysql.database
});

exports.sqlConfig = function () {
	conSql.connect(function (err) {
		if (err) {
			console.log(err);
			if (err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
				sqlConfig(); // lost due to either server restart, or a
			}
		} else {
			setInterval(function () {
			  	conSql.query("select * from users where client='innix'");
			}, 10000);
		}
	});
	return conSql;
}

exports.productApi = function (request, response) {
	let sql = "select * from enotify where guid='" + request.params.userGUID + "'";

	conSql.query(sql, function (err, result) {
		if (err || result.length == 0) {
			response.status(400).send({
				error: "Nenhum produto encontrado, verifique a chave do usuário"
			});
		} else {
			response.status(200).send(result);
		}
	});
}

exports.userConfig = function (request, response) {
	let sql = "select * from fcmconfig where guid='" + request.params.userGUID + "'";

	conSql.query(sql, function (err, result) {
		if (err || result.length == 0) {
			response.status(400).send({
				error: "Cliente não encontrado ou não existente"
			});
		} else {
			response.status(200).send(result);
		}
	});
}

exports.createStore = function (request, response) {
	var sql = "insert into users(guid, client, password, host, email) values ?";
	var values = [
		[request.query.guid, request.query.client, request.query.password, request.query.host, request.query.email]
	];

	if (!request.query.guid || !request.query.client || !request.query.password || !request.query.host || !request.query.email) {
		response.status(404).send({
			status: 'Valores inseridos não estão corretos'
		});
	} else {
		conSql.query(sql, [values], function (err, result) {
			if (err) response.status(400).send({
				error: err
			});
			else {
				response.status(200).send({
					status: 'Cliente Cadastrado com sucesso'
				});
			}
		});
	}
}

exports.createProduct = function (request, response) { // Create a product
	var sql = "insert into enotify(guid,client,host,product,device,status,name,title,icon,image,body,timeout,link,buttonview,iconview,schedulehour,utm,utmstate) values ?";
	let status = (request.body.status) ? 'on' : 'off';
	var values = [[request.body.guid, request.body.client, request.body.host, request.body.type, request.body.device, status, request.body.name, request.body.title, request.body.icon, request.body.image,
	request.body.body, request.body.timeout, request.body.link, request.body.view, request.body.iconview, request.body.dateTime, request.body.utm, request.body.utmStatus
	]];

	conSql.query(sql, [values], function (err, result) {
		if (err) response.status(400).send({
			error: err
		});
		else response.status(200).send({
			status: 'Produto cadastrado com sucesso'
		});
	});
}

exports.changeProduct = function (request, response) {
	var sql = "update enotify set guid=?,client=?,host=?,product=?,device=?,status=?,name=?,title=?,icon=?,image=?,body=?,timeout=?,link=?,buttonview=?,iconview=?,schedulehour=?,utm=?,utmstate=? where id=" + request.body.idProd;
	let status = (request.body.status) ? 'on' : 'off';
	var values = [request.body.guid, request.body.client, request.body.host, request.body.type, request.body.device, status, request.body.name, request.body.title, request.body.icon, request.body.image,
	request.body.body, request.body.timeout, request.body.link, request.body.view, request.body.iconview, request.body.dateTime, request.body.utm, request.body.utmStatus, request.body.idProd
	];

	conSql.query(sql, values, function (err, result) {
		if (err) response.status(400).send({
			error: err
		});
		else response.status(200).send({
			status: 'Produto alterado com sucesso'
		});
	});
}

exports.deleteProduct = function (request, response) {
	var sql = "delete from enotify where id =" + request.params.idProd;

	conSql.query(sql, function (err, result) {
		if (err) response.status(400).send({
			error: err
		});
		else response.status(200).send({
			status: 'Produto deletado com sucesso'
		});
	});
}

/** Integration FCM Page **/
exports.fcmcheck = function (request, response) {
	let sql = "select * from fcmconfig where guid='" + request.params.userGUID + "'";

	conSql.query(sql, function (err, result) {
		if (err) {
			response.status(500).send({
				error: "Ocorreu um problema na requisição com o banco de dados. Reinicie a página ou tente mais tarde."
			});
		} else if(result.length == 0){
			response.status(400).send({
				message: "Não há fcm cadastrado para este cliente"
			});
		} else {
			response.status(200).send(result);
		}
	});
}

exports.fcmconfig = function (request, response) {
	var sql = 'insert into fcmconfig (guid,fcmproj,fcmapi,fcmtoken,fcmsender) values (?) ON DUPLICATE KEY UPDATE fcmproj="'+ request.body.fcmproj +'",fcmapi="'+ request.body.fcmapi +'",fcmtoken="'+ request.body.fcmtoken +'",fcmsender="'+ request.body.fcmsender +'"';
	var values = [request.body.guid, request.body.fcmproj, request.body.fcmapi, request.body.fcmtoken, request.body.fcmsender];

	conSql.query(sql, [values], function (err, result) {
		if (err) response.status(400).send({
			error: err
		});
		else response.status(200).send({
			status: 'Fcm foi cadastro com sucesso'
		});
	});
}
/** ------- **/