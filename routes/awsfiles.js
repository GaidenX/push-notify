
const express = require('express');
const app = express();
const fs = require('file-system');
const path = require('path');
const AWS = require('aws-sdk');
const config = require('../config/config.json');

const s3 = new AWS.S3({
	accessKeyId: config.AWS.accessKeyId,
	secretAccessKey: config.AWS.secretAccessKey
});

exports.awsFileUpload = function(newpath,userName,fileName){
	fs.readFile(newpath, (err, data) => {
		if (err) console.log(err);
		const aws_params = {
			Bucket: 'push-enotify',
			Key: userName+'/'+fileName,
			Body: data
	   };

		s3.upload(aws_params, function(s3Err, data) {
		  if (s3Err) throw s3Err
			  console.log(`File uploaded successfully at ${data.Location}`)
		 });
	});
}

exports.updateApp = function (request, response) {
	const params = {
		Bucket: 'push-enotify'
	};

	s3.listObjects(params, function (err, data) {
		if (err) response.status(400).send({
			status: err
		});
		else {
			for (key in data.Contents) {
				if (data.Contents[key].Size != 0) {
					const pasteAws = data.Contents[key].Key.split('/')[0];
					const fileAws = data.Contents[key].Key.split('/')[1];

					s3.getObject({Bucket: 'push-enotify',Key: data.Contents[key].Key}, (err, data) => {
						if (err) console.error(err)
                        const filePath = path.join(__dirname, '../public/upload/') + pasteAws + '/' + fileAws;
						fs.writeFileSync(filePath, data.Body)
					})
				}
			}
			response.status(200).send({
				status: 'API atualizada com sucesso!'
			});
		}
	});
}